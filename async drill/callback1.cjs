const board_1 = require("../boards_1.json");

function getBoardInfoById(id, getBoardCallBack) {
  setTimeout(() => {
    if (!id) {
      let error = new Error("error occured in id");
      return getBoardCallBack(true, null, error.message);
    } else {
      let result = board_1.filter((each) => {
        return each.id == id;
      });
      return getBoardCallBack(null, result, null);
    }
  }, 2000);
}

/* let id = "mcu453ed";

let ans = getBoardInfoById(id, (err, data, error) => {
  if (err) {
    return error;
  } else {
    return data;
  }
});
console.log("27 ", ans); */

module.exports = getBoardInfoById;
