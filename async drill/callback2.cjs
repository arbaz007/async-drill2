const list_1 = require("../lists_1.json");
const board_1 = require("../boards_1.json");

function getListByBoardId(boardId, getList) {
  if (!boardId) {
    let error = new Error("Error occured");
    getList(true, null, error.message);
  } else {
    setTimeout(() => {
      let result = Object.keys(list_1).reduce((acc, curr) => {
        if (curr == boardId) {
          acc = list_1[curr];
        }
        return acc;
      }, []);
      getList(null, result, null);
    }, 2000);
  }
}



module.exports = getListByBoardId;
