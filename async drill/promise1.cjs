const board_1 = require("../boards_1.json");

function getBoardInfoById(id) {
  return new Promise((resolve, reject) => {
    if (!id) {
      let error = new Error("error occured in id");
      reject(error);
      return;
    }
    setTimeout(() => {
      let result = board_1.filter((each) => {
        return each.id == id;
      });
      resolve(result);
    }, 2000);
  });
}



module.exports = getBoardInfoById