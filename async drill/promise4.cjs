const getBoardInfoById = require("../async drill/promise1.cjs");
const getListByBoardId = require("../async drill/promise2.cjs");
const getCardsByListId = require("../async drill/promise3.cjs");

function getBoardInfo(thanosBoardId) {
  getBoardInfoById(thanosBoardId)
    .then((res) => {
      console.log(res);
    })
    .catch(err => console.log("error: ", err));

  getListByBoardId(thanosBoardId).then((data) => {
    console.log(data);
    data.forEach((each) => {
      if (each.name == "Mind") {
        getCardsByListId(each.id).then (res => {
          console.log(res);
        }).catch (err => {console.log("error: ", err);})
      }
    });
  }).catch (err => {
    console.log("error: ", err);
  })
}


module.exports = getBoardInfo;
