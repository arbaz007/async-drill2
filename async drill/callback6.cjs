const getListByBoardId =  require("./callback2.cjs");
const getCardsByListId =  require("./callback3.cjs");

const getBoardInfoById = require("../async drill/callback1.cjs");



function callback6(thanosBoardId) {
  getBoardInfoById(thanosBoardId, (err, data, error) => {
    if (err) {
      console.log(error);
    } else {
      console.log(data);
    }
  });
  getListByBoardId(thanosBoardId, (err, data, error) => {
    if (err) {
      console.log(error);
    } else {
      console.log(data);
      data.forEach (each => {
        getCardsByListId(each.id, (err, data, error ) => {
            if (err) {
                console.log(error);
            }else {
                console.log(data);
            }
        })
      })
    }
  });
}


module.exports = callback6