const cards_1 = require("../cards_1.json");

function getCardsByListId(listId) {
  return new Promise((resolve, reject) => {
    if (!listId) {
      let error = new Error("Error in listId");
      reject(error);
      return
    } else {
      setTimeout(() => {
        let result = Object.keys(cards_1).reduce((acc, curr) => {
          if (curr == listId) {
            acc = cards_1[curr];
          }
          return acc;
        }, []);
        resolve(result);
      }, 2000);
    }
  });
}



module.exports = getCardsByListId
