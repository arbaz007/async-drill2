const list_1 = require("../lists_1.json");

function getListByBoardId(boardId) {
  return new Promise((resolve, reject) => {
    if (!boardId) {
      let error = new Error("Error occured");
      reject(error);
      return;
    } else {
      setTimeout(() => {
        let result = Object.keys(list_1).reduce((acc, curr) => {
          if (curr == boardId) {
            acc = list_1[curr];
          }
          return acc;
        }, []);
        resolve(result);
      }, 2000);
    }
  });
}



module.exports = getListByBoardId