const getListByBoardId = require("./callback2.cjs");
const getCardsByListId = require("./callback3.cjs");
const list_1 = require("../lists_1.json");
const getBoardInfoById = require("../async drill/callback1.cjs");



function getBoardInfo(thanosBoardId) {
  getBoardInfoById(thanosBoardId, (err, data, error) => {
    if (err) {
      console.log(error);
    } else {
      console.log(data);
    }
  });
  getListByBoardId(thanosBoardId, (err, data, error) => {
    if (err) {
      console.log(error);
    } else {
      console.log(data);
      data.forEach((each) => {
        if (each.name == "Mind") {
          getCardsByListId(each.id, (err, data, error) => {
            if (err) {
              console.log(error);
            } else {
              console.log(data);
            }
          });
        }
      });
    }
  });
}



module.exports = getBoardInfo