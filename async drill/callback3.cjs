const cards_1 = require("../cards_1.json");
const list_1 = require("../lists_1.json");

function getCardsByListId(listId, getCard) {
  if (!listId) {
    let error = new Error("Error in listId");
    getCard(true, null, error.message);
  } else {
    setTimeout(() => {
      let result = Object.keys(cards_1).reduce((acc, curr) => {
        if (curr == listId) {
          acc = cards_1[curr];
        }
        return acc;
      }, []);
      getCard(null, result, null);
    }, 2000);
  }
}



module.exports = getCardsByListId;
